package com.yenlo.apim;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.apimgt.api.APIManagementException;
import org.wso2.carbon.apimgt.api.model.API;
import org.wso2.carbon.apimgt.api.model.Subscriber;
import org.wso2.carbon.apimgt.impl.APIConstants;
import org.wso2.carbon.apimgt.impl.dao.ApiMgtDAO;
import org.wso2.carbon.apimgt.impl.utils.APIUtil;
import org.wso2.carbon.context.CarbonContext;
import org.wso2.carbon.governance.api.generic.GenericArtifactManager;
import org.wso2.carbon.governance.api.generic.dataobjects.GenericArtifact;
import org.wso2.carbon.governance.registry.extensions.interfaces.Execution;
import org.wso2.carbon.registry.core.jdbc.handlers.RequestContext;
import org.wso2.carbon.user.api.UserStoreException;
import org.wso2.carbon.user.api.UserStoreManager;

/**
 * Sends notification email to the subscribers of the API
 *
 */
public class NotificationExecutor implements Execution {

	private static final Log log = LogFactory.getLog(NotificationExecutor.class);

	public static final String EMAIL_CLAIM_URI = "http://wso2.org/claims/emailaddress";

	private String username;
	private String password;
	private String host;
	private Properties properties = new Properties();

	@Override
	public boolean execute(RequestContext context, String currentState, String targetState) {
		boolean result = false;
		try {
			GenericArtifactManager artifactManager = APIUtil.getArtifactManager(context.getSystemRegistry(),
					APIConstants.API_KEY);
			String artifactId = context.getResource().getUUID();
			GenericArtifact apiArtifact = artifactManager.getGenericArtifact(artifactId);
			API api = APIUtil.getAPI(apiArtifact);
			List<String> emails = getAllSubscribersEmails(api);
			if (emails.isEmpty()) {
				log.info("No subscribers found for API " + api.getId());
			} else {
				String message = "Published API " + api.getId().getApiName() + " " + api.getId().getVersion();
				sendEmail(emails, message);
			}
			result = true;
		} catch (Exception e) {
			log.error("Failed to send notification emails while executing NotificationExecutor", e);
		}
		return result;
	}

	private void sendEmail(List<String> users, String emailBody) throws AddressException, MessagingException {
		Session session = Session.getDefaultInstance(properties, null);

		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(username));
		for (String user : users) {
			message.addRecipient(Message.RecipientType.BCC, new InternetAddress(user));
		}
		message.setSubject("WSO2 API Manager Lifecycle Update");
		message.setContent(emailBody, "text/html");

		Transport transport = session.getTransport("smtp");
		transport.connect(host, username, password);
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
	}

	private List<String> getAllSubscribersEmails(API api) throws APIManagementException, UserStoreException {
		UserStoreManager userstoremanager = CarbonContext.getThreadLocalCarbonContext().getUserRealm()
				.getUserStoreManager();
		List<String> emails = new ArrayList<>();
		for (Subscriber subscriber : ApiMgtDAO.getInstance().getSubscribersOfAPI(api.getId())) {
			String email = userstoremanager.getUserClaimValue(subscriber.getName(), EMAIL_CLAIM_URI, null);
			if (email != null && !email.trim().isEmpty()) {
				emails.add(email);
			}
		}
		return emails;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void init(Map parameterMap) {
		if (parameterMap != null) {
			properties.put("mail.smtp.port", parameterMap.get("port"));
			properties.put("mail.smtp.auth", parameterMap.get("auth"));
			properties.put("mail.smtp.starttls.enable", parameterMap.get("tls"));
			username = (String) parameterMap.get("username");
			password = (String) parameterMap.get("password");
			host = (String) parameterMap.get("host");
		}
	}

}
